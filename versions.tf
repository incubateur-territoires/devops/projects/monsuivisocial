terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.12.0"
    }
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.8.0"
    }
    grafana = {
      source  = "grafana/grafana"
      version = "~> 1.27.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.3.2"
    }
  }
  required_version = "~> 1.7.2"
}
